#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget),
    Datablock(0)
{
    ui->setupUi(this);
    antserver = new QTcpServer(this);
    antserver->listen(QHostAddress::Any,(qint16)6415);
    connect(antserver, SIGNAL(newConnection()), this, SLOT(openConnection()));
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_pushButton_clicked()
{
    antserver->close();
    ui->textEdit->append("Server is close");
}

void Widget::openConnection()
{
    QTcpSocket * antsock = antserver->nextPendingConnection();
    ui->textEdit->append("We have new user!");
    connect(ui->pushButton, SIGNAL(clicked()), antsock, SLOT(disconnectFromHostImplementation()));
    connect(antsock, SIGNAL(disconnected()), antsock, SLOT(deleteLater()));
    connect(antsock, SIGNAL(readyRead()), this, SLOT(readClient()));
    sendTOclient(antsock, "You are connecting! \n\r");
}

void Widget::on_pushButton_2_clicked()
{
    antserver = new QTcpServer(this);
    antserver->listen(QHostAddress::Any,(qint16)6415);
    connect(antserver, SIGNAL(newConnection()), this, SLOT(openConnection()));
    ui->textEdit->append("Server is open");
}

void Widget::sendTOclient(QTcpSocket *a, const QString& str)
{
    QByteArray datablock;
    QDataStream output(&datablock, QIODevice::WriteOnly);
    output.setVersion(QDataStream::Qt_4_7);
    output << str;
//    output.device()->seek(0);
//    output << quint16(datablock.size()-sizeof(quint16));
    a->write(datablock);
}

void Widget::readClient()
{
    QTcpSocket * antsocket = (QTcpSocket*)sender();
//    QDataStream input(&antsocket, QIODevice::ReadOnly);
    QDataStream input(antsocket);
    input.setVersion(QDataStream::Qt_4_7);
    sendTOclient(antsocket, "Your message going\n\r");
    ui->textEdit->append("User gives command");
//    input<< "test string\r\n";
//    input>>Datablock;
    QString str;
    char bst[11];
//    input.writeRawData(&str, 10);
    int len=input.readRawData(bst, 11);
    bst[len-1]='\0';
//    bst[len]='\b';

    //str="qwerty";
//    input >> str;
//    input << str << "\r\n";
//    for(int i=0; i < 1; i++)
////    for(;;)
//    {
//        if(!Widget::Datablock)
//        {
//            if(antsocket->bytesAvailable() < sizeof(quint16))
//            {
//                sendTOclient(antsocket, "socket smaller for quint16\n\r");
////                break;
//            }
//            else
//            {
//                input >> Datablock;
//                sendTOclient(antsocket, "Stream was inputed\n\r");
//            }
//        }
//        if (antsocket->bytesAvailable() > Datablock)
//        {
//            sendTOclient(antsocket, "socket bigger for Datablock");
//            break;
//        }
//        QString str;
//        input >> str;
//        QString str2;
//        str2 += str;
        str=bst;
        str=str.left(str.length()-1);
        ui->textEdit->append(str);
//        ui->textEdit->setText(ui->textEdit->append(bst));
//        ui->pushButton->setText(str2);
//        sendTOclient(antsocket, "Server receive your message:\n\r"); /*"+ str +"*/
//    }
//    Datablock=0;
}
