#-------------------------------------------------
#
# Project created by QtCreator 2011-03-30T20:27:11
#
#-------------------------------------------------

QT       += core gui
QT           += network

TARGET = server
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp

HEADERS  += widget.h

FORMS    += widget.ui
