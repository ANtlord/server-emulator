#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QtNetwork>

namespace Ui {
    class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = 0);
    ~Widget();

private:
    Ui::Widget *ui;
    QTcpServer * antserver;
    quint16 Datablock;

    void sendTOclient(QTcpSocket * a, const QString &str);


private slots:
    void on_pushButton_2_clicked();
    void on_pushButton_clicked();
    virtual void openConnection();
    void readClient();

};

#endif // WIDGET_H
